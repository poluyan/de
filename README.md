## Информация для групп ДУПЗ (ИСАУ)

Здесь будет представлена вся информация по практическим заданиям.

Контакты (по всем вопросам):

* telegram: [@sergpsv](https://t.me/sergpsv)
* e-mail: `psv-dubna@mail.ru`

[Книги](https://pastebin.com/N1w3isRD)

## Результаты

[2251](https://docs.google.com/spreadsheets/d/1txnJDoUcaRZXEcTfkz-bg5qBycoxbLBpPGs7G6JDpzQ/edit?usp=sharing)

[2252](https://docs.google.com/spreadsheets/d/1EoX1v2ksT0esO8XJP_WYqgrVoWns795jq2w4KBbMEh0/edit?usp=sharing)

[2253](https://docs.google.com/spreadsheets/d/1OihaO2AuE3scRpY855STGj4iJT3zJUWDCKCTMGyKEs0/edit?usp=sharing)
